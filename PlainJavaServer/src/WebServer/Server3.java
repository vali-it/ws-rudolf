package WebServer;

import WebServer.framework.BusinessLogicController;
import WebServer.framework.WebPath;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;

import java.util.HashMap;
import java.util.Map;

public class Server3 {

    public static void main(String[] args) throws IOException {

        try (ServerSocket serverSocket = new ServerSocket(8081)) {
            while (true) {
                try (Socket client = serverSocket.accept()) {
                    responseBodyString(client);
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void responseBodyString(Socket client)
            throws InvocationTargetException,
            IllegalAccessException, IOException {

        String httpRequest = getHttpRequest(client);
        Map<String, Method> methodMap = getMethods();
        String path = getPath(httpRequest);
        Map<String, String> headers = httpHeaders(httpRequest);

        if (path == null) {
            return;
        }

        Method matchingMethod = null;
        for (Map.Entry<String, Method> entry : methodMap.entrySet()) {
            if (path.startsWith(entry.getKey())) {
                matchingMethod = entry.getValue();
                break;
            }
        }

        if (matchingMethod == null) {

        } else {
            Object invoke = matchingMethod.invoke(new BusinessLogicController(),
                    "",
                    path,
                    headers);
            byte[] response = (byte[]) invoke;
            client.getOutputStream().write(response);
        }
    }

    public static Map<String, Method> getMethods() {
        Map<String, Method> methodMap = new HashMap();
        for (Method method : BusinessLogicController.class.getDeclaredMethods()) {
            WebPath annotation = method.getAnnotation(WebPath.class);
            methodMap.put(annotation.path(), method);
        }
        return methodMap;
    }

    public static String getHttpRequest(Socket client) throws IOException {
        BufferedReader buffread = new BufferedReader(new InputStreamReader(client.getInputStream()));

        StringBuilder requestBuilder = new StringBuilder();
        String line = buffread.readLine();
        int count = 0;

        while (line != null) {
            if (line.isEmpty()) {
                count++;
                if (count == 2) {
                    break;
                }
                line = buffread.readLine();
                continue;
            }
            requestBuilder.append(line + "\n");
            line = buffread.readLine();
        }

        String httpRequest = requestBuilder.toString();
        System.out.println(requestBuilder);
        return httpRequest;
    }

    public static String getPath(String httpRequest) throws IOException {

        if (httpRequest.isBlank()) {
            return null;
        }
        String[] requestLines = httpRequest.split("\n");
        String[] requestLine = requestLines[0].split(" ");
        String path = requestLine[1];

        return path;
    }

    public static Map<String, String> httpHeaders(String httpRequest) throws IOException {

        Map<String, String> headers = new HashMap<>();
        String[] headerLines;
        String[] requestLines = httpRequest.split("\n");
        String requestBody;

        for (int i = 1; i < requestLines.length; i++) {
            if (!requestLines[i].isBlank()) {
                headerLines = requestLines[i].split(":");
                headers.put(headerLines[0], headerLines[1]);
            } else {
                requestBody = requestLines[i + 1];
                System.out.println(requestBody);
                break;

            }
        }
        return headers;
    }
}