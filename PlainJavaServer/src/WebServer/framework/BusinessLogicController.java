package WebServer.framework;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

public class BusinessLogicController {

    @WebPath(path = "/hello")
    public byte[] printHello(String requestBody, String path, Map<String, String> headers) {
        StringBuilder rb = new StringBuilder();
        rb.append("HTTP/1.1 200 OK \n");
        rb.append("ContentType: text/html\n");
        rb.append("\n\n");

        rb.append("Hello World. Timestamp is ")
                .append(System.currentTimeMillis())
                .append("and your browser is ").append(
                        headers.get("User-Agent")
                );

        return rb.toString().getBytes(StandardCharsets.UTF_8);
    }

    @WebPath(path = "/static-content")
    public byte[] staticContent(String requestBody, String path, Map<String, String> headers) throws IOException {

        String[] methodAndFile = path.split("/");
        if (methodAndFile.length == 2) {
            StringBuilder responseBody = new StringBuilder();
            responseBody.append("HTTP/1.1 404 Not Found \n");
            responseBody.append("ContentType: " + "text/html");
            responseBody.append("\n\n");
            return responseBody.toString().getBytes(StandardCharsets.UTF_8);
        }
        Path filename = Paths.get("inputFiles", methodAndFile[2]);
        String contentType = Files.probeContentType(filename);

        StringBuilder responseBody = new StringBuilder();
        responseBody.append("HTTP/1.1 200 OK \n");
        responseBody.append("ContentType: " + contentType);
        responseBody.append("\n\n");
        Files.readAllBytes(filename);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        baos.write(responseBody.toString().getBytes(StandardCharsets.UTF_8));
        baos.write(Files.readAllBytes(filename));

        return baos.toByteArray();
    }

    @WebPath(path = "/favicon.ico")
    public byte[] favicon(String requestBody, String path, Map<String, String> headers) {
        return new byte[]{};
    }

    @WebPath(path = "/salvesta")
    public byte[] salvesta(String requestBody, String path, Map<String, String> headers) {
        StringBuilder rb = new StringBuilder();
        rb.append("HTTP/1.1 200 OK \n");
        rb.append("ContentType: text/html\n");
        rb.append("\n\n");

        rb.append("Hello World. Timestamp is ")
                .append(System.currentTimeMillis())
                .append("and your browser is")
                .append(headers.get("User-Agent"));

        return rb.toString().getBytes(StandardCharsets.UTF_8);
    }

    @WebPath(path = "/foo")
    public byte[] foo(String requestBody, String path, Map<String, String> headers) {

        StringBuilder rb = new StringBuilder();
        rb.append("HTTP/1.1 200 OK \n");
        rb.append("ContentType: text/html\n");
        rb.append("\n\n");

        rb.append("Hello World. Timestamp is ")
                .append(System.currentTimeMillis())
                .append("and your browser is").append(
                        headers.get("User-Agent")
                );

        return rb.toString().getBytes(StandardCharsets.UTF_8);
    }

}
