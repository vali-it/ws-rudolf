package WebServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Server2 {

    public static void main(String[] args) throws IOException {

        try (ServerSocket serverSocket = new ServerSocket(8081)) {
            while (true) {
                try (Socket client = serverSocket.accept()) {
                    findFile(client);
                }
            }
        }
    }

    public static void findFile(Socket client) throws IOException {
        BufferedReader buffread = new BufferedReader(new InputStreamReader(client.getInputStream()));

        StringBuilder requestBuilder = new StringBuilder();
        String line;
        while (!(line = buffread.readLine()).isBlank()) {
            requestBuilder.append(line + "\n");
        }

        String httpRequest = requestBuilder.toString();
        String[] requestLines = httpRequest.split("\n");
        String[] requestLine = requestLines[0].split(" ");
        String pathString = requestLine[1];


        getFilePath(pathString);

        Path path = getFilePath(pathString);
        String status;
        if (Files.exists(path)) {
            status = "200 OK";
            compileResponse(client, status, getContentType(path), Files.readAllBytes(path));
        } else {
            status = "404 Not Found";
            byte[] errorContent = "<h1>404</h1><br><h1>404</h1><br><h1>404</h1>".getBytes();
            compileResponse(client, status, "text/html", errorContent);
        }
    }


    public static Path getFilePath(String pathString) {
        String newPathString = pathString.substring(1);
        Path path = Paths.get("inputFiles/" + newPathString);
        return path;
    }

    public static String getContentType(Path path) throws IOException {
        String contentType = Files.probeContentType(path);
        return contentType;
    }

    public static void compileResponse(
            Socket client,
            String status,
            String contentType,
            byte[] content) throws IOException {
        OutputStream clientOutput = client.getOutputStream();
        clientOutput.write(("HTTP/1.1 " + status + "\n").getBytes());
        clientOutput.write(("ContentType: " + contentType + "\n").getBytes());
        clientOutput.write("\n".getBytes());
        clientOutput.write(content);
        clientOutput.write("\n\n".getBytes());
        clientOutput.flush();
        client.close();
    }
}



